FROM registry.gitlab.com/k2273/dev-school-app/build:build17
COPY dev-school-app-1.0-SNAPSHOT.jar .
EXPOSE 8080
CMD java -jar dev-school-app-1.0-SNAPSHOT.jar
